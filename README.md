# README #

Lecture notes for Web Application Architecture course on Coursera.

### How do I get set up? ###

* Two files have been provided - an .odt file that can be edited using LibreOffice Writer, OpenOffice suite, etc. and a .docx file that can be used in Microsoft Word. 

* Feel free to add/edit the notes

* Originally created using LibreOffice Writer, so the associated .docx version may have some formatting issues. 

### Who do I talk to? ###

* Send a pull request to the Repo owner to add your version to the master
